from django.db import models
from accounts.models import CustomUser
#from projects.models import Project
#from business.models import Supplier
from datetime import datetime

class Movement(models.Model):
    payee = models.CharField(
        max_length=254
    )
    creation_date = models.DateField(
        default = datetime.now,
    )
    description = models.CharField(
        max_length=254
    )
    amount = models.DecimalField(
        decimal_places=3,
        max_digits=7
    )
    no_invoice = models.BooleanField(
        blank = True,
        default = False
    )
    payment = models.BooleanField(
        blank = True,
        default = False
    )
    responsable = models.ForeignKey(
        CustomUser,
        models.CASCADE,
        related_name='%(class)s_responsable',
    )
    files = models.FileField(
        blank = True,
        null = True
    )
    
    class Meta:
        permissions = [
            ("do_payments", "Can do payments"),
        ]
    
    def __str__(self):
        return self.payee