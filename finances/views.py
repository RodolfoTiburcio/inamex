from django.db.models import Sum

from django.views.generic import (
    CreateView, UpdateView, DeleteView, ListView,
    RedirectView, MonthArchiveView
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Movement
from .forms import MovementCreateForm
from accounts.models import CustomUser

import datetime

class MovementCreateView(LoginRequiredMixin,CreateView):
    model = Movement
    #fields = '__all__'
    form_class = MovementCreateForm
    success_url = reverse_lazy("finances:finances_home")

    def get_form_kwargs(self):
        kwargs = super(MovementCreateView, self).get_form_kwargs()
        u = self.request.user
        kwargs['initial']= {
            'responsable':u.pk,
        }
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context =  super(MovementCreateView, self).get_context_data(*args, **kwargs)
        context['no_invoice'] = Movement.objects.filter(
            no_invoice = True,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        )
        context['invoice'] = Movement.objects.filter(
            no_invoice = False,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        )
        context['total_fact'] = context['invoice'].filter(
            no_invoice=False
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        context['total_no_fact'] = context['no_invoice'].filter(
            no_invoice = True
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        return context

class MovementUpdateView(LoginRequiredMixin, UpdateView):
    model = Movement
    #fields = '__all__'
    form_class = MovementCreateForm
    success_url = reverse_lazy("finances:finances_home")

    def get_context_data(self, *args, **kwargs):
        context =  super(MovementUpdateView, self).get_context_data(*args, **kwargs)
        context['no_invoice'] = Movement.objects.filter(
            no_invoice = True,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        )
        context['invoice'] = Movement.objects.filter(
            no_invoice = False,
            responsable = self.request.user.pk,
            creation_date__month = datetime.datetime.now().month,
        )
        context['total_fact'] = context['invoice'].filter(
            no_invoice=False
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        context['total_no_fact'] = context['no_invoice'].filter(
            no_invoice = True
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        return context

class MovementDeleteView(LoginRequiredMixin, DeleteView):
    model = Movement
    success_url = reverse_lazy("finances:finances_home")

class MovementAdminListView(LoginRequiredMixin, ListView):
    model = Movement
    # context_object_name = 'movement_list'
    
    def get_context_data(self, **kwargs):
        usuario = self.kwargs['usr']
        context =  super(MovementAdminListView, self).get_context_data(**kwargs)
        
        context["next_user"] = CustomUser.objects.filter(id__gt=usuario).order_by('id').first()
        context["prev_user"] = CustomUser.objects.filter(id__lt=usuario).order_by('-id').first()
        
        context["usuario"] = CustomUser.objects.get(pk=usuario)
        
        context["month_movements"] = context["usuario"].movement_responsable.all().filter(
            creation_date__year=self.kwargs['year'],
            creation_date__month=self.kwargs['month'],
        )
        
        context["next_month_movements"] = context["usuario"].movement_responsable.all().filter(
            creation_date__year=self.kwargs['year'],
            creation_date__month__gt=self.kwargs['month'],
        ).order_by('id').first()
        context["prev_month_movements"] = context["usuario"].movement_responsable.all().filter(
            creation_date__year=self.kwargs['year'],
            creation_date__month__lt=self.kwargs['month'],
        ).order_by('-id').first()
        
        if context["month_movements"].first():
            context['first_date'] = context["month_movements"].first().creation_date
        else:
            context['first_date'] = datetime.datetime.now()
        
        context['total_fact'] = context["month_movements"].filter(
            no_invoice=False,
            payment = False,
        ).aggregate(
            Sum('amount') 
        )['amount__sum']
        
        context['total_no_fact'] = context["month_movements"].filter(
            no_invoice = True,
            payment = False,
        ).aggregate(
            Sum('amount')
        )['amount__sum']
        
        fact_ids = context["month_movements"].filter(
            no_invoice=False,
            payment = False,
        )
        
        no_fact_ids = context["month_movements"].filter(
            no_invoice=True,
            payment = False,
        )
        # Create Fact_ids str
        context['fact_ids'] = ""
        for fact_id in fact_ids:
            context['fact_ids'] += str(fact_id.pk)
            context['fact_ids'] += ","
        context['fact_ids'] = context['fact_ids'][:-1]
        # Create no_Fact_ids str
        context['no_fact_ids'] = ""
        for no_fact_id in no_fact_ids:
            context['no_fact_ids'] += str(no_fact_id.pk)
            context['no_fact_ids'] += ","
        context['no_fact_ids'] = context['no_fact_ids'][:-1]
        return context

class PaymentRedirectView(RedirectView):
    try:
        movimiento = Movement.objects.all().order_by('-id').first()
    except:
        movimiento = None
    print(movimiento)
    if not movimiento:
        url = reverse_lazy("finances:finances_validate", kwargs={
            'usr':'1',
            'year':'2022',
            'month':'1',
        })
    else:
        url = reverse_lazy("finances:finances_validate", kwargs={
            'usr':movimiento.responsable.pk,
            'year':movimiento.creation_date.year,
            'month':movimiento.creation_date.month,
        })
    
class MakePaymentFunc(LoginRequiredMixin, RedirectView):
    url = reverse_lazy("finances:finances_home")
    
    def get_redirect_url(self, *args, **kwargs):
        por_pagar = [int(x) for x in self.request.GET.get('paymentlist').split(",")]
        for mov_id in por_pagar:
            m = Movement.objects.get(pk=mov_id)
            m.payment = True
            m.save()
        return super().get_redirect_url(*args, **kwargs)