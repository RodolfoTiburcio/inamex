from django import forms
from django.forms import ModelChoiceField
from django.contrib.auth.models import User
from datetime import datetime
from finances.models import Movement

class MovementCreateForm(forms.ModelForm):
    class Meta:
        model = Movement
        fields = '__all__'
        widgets = {
            'payee':forms.TextInput(attrs={'class':'form-control'}),
            'creation_date':forms.DateInput(attrs={'class':'form-control datepicker','type':'date'}),
            'description':forms.Textarea(attrs={'class':'form-control'}),
            'amount':forms.NumberInput(attrs={'class':'form-control'}),
            'no_invoice':forms.NullBooleanSelect(attrs={'class':'form-control'}),
            'payment':forms.NullBooleanSelect(attrs={'class':'form-control'}),
            'responsable':forms.Select(attrs={'class':'form-control'}),
            'files':forms.FileInput(attrs={'class':'form-control'}),
        }

'''class ProjectSelect(ModelChoiceField):
    def label_from_instance(self, obj):
        return "{}".format(
            obj.name
        )

class UserSelect(ModelChoiceField):
    def label_from_instance(self,obj):
        return '{}'.format(
            obj.username
        )

class PayeeSelect(ModelChoiceField):
    def label_from_instance(self, obj):
        return '{}'.format(
            obj.name
        )

class NewMovement(forms.Form):
    payee = PayeeSelect(
        label = 'Proveedor',
        queryset=Supplier.objects.all(),
        required = False,
    )

    creation_date = forms.DateField(
        label = "Fecha", 
        widget = forms.DateInput(
            format=('%m/%d/%Y'),
            attrs={
                'class':'datepicker',
                'type':'date',
            },
        )
    )
    project = ProjectSelect(
        label = "Proyecto",
        queryset = Project.objects.all(),
        required = False
    )
    description = forms.CharField(
        label = "Descripcion",
        max_length = 254,
        widget = forms.Textarea(
            attrs={
                'cols':'30',
                'rows':'2'
            }
        )
    )
    amount = forms.DecimalField(
        label = "Monto",
        decimal_places=3,
        max_digits=7
    )
    no_invoice = forms.BooleanField(
        label = "Sin factura",
        required = False,
        initial = False
    )
    payment = forms.BooleanField(
        label = "Pagado",
        required = False,
        initial = False
    )
    account = UserSelect(
        label = "Responsable",
        queryset = User.objects.all(),
    )
    files = forms.FileField(
        label = "Archivos",
        max_length=60,
        required = False
    )'''