from django.urls import path
from .views import MovementCreateView, MovementUpdateView, MovementDeleteView, MovementAdminListView, MakePaymentFunc, PaymentRedirectView

app_name = 'finances'

urlpatterns = [
    path('', MovementCreateView.as_view(), name = 'finances_home'),
    path('edit/<int:pk>', MovementUpdateView.as_view(), name = 'finances_edit'),
    path('delete/<int:pk>', MovementDeleteView.as_view(), name='finances_delete'),
    path('validate/<int:usr>/<int:year>/<int:month>/', MovementAdminListView.as_view(), name = 'finances_validate'),
    path('pagar_fact/', MakePaymentFunc.as_view(), name = 'finances_pay'),
    path('lastmov/', PaymentRedirectView.as_view(), name='finances_last'),
]