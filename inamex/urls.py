from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from allauth.account.views import LoginView, SignupView

urlpatterns = [
    # django admin app
    path('app/_dft_wedb4p__eds/', admin.site.urls),
    # user management app
    path('app/accounts/', include('allauth.urls')),
    path('app/login/', LoginView.as_view()),
    path('app/signup/', SignupView.as_view()),
    # local apps
    path('app/', include('pages.urls')),
    path('app/finances/', include('finances.urls'),)
] + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)
